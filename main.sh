#!/bin/bash

HOUR=${CC_HOUR:-"*"}
MINUTES=${CC_MINUTES:-"*"}
COMMAND=${CC_CMD:-"echo YOU DIDN'T SAY THE MAGIC WORD"}

[[ $HOUR != "*" ]] && HOUR=$(printf "%02d" $HOUR)
[[ $MINUTES != "*" ]] && MINUTES=$(printf "%02d" $MINUTES)
	
echo "Sched: $HOUR:$MINUTES"
		
while true;
do
	CURR_HOUR=$(date +%H)
	CURR_MINS=$(date +%M)
	if [[ "$CURR_HOUR:$CURR_MINS" == $HOUR:$MINUTES ]]
	then
		echo "$(date)    Running job..."
		$COMMAND
	fi
	sleep $(( 60 - 10#$(date +%S) ))
done
